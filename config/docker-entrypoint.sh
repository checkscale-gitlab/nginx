#!/bin/bash
set -e

# This copy will avoid using preconfigured files.
# Environment changes will be done at each runs.
cp /default.conf /etc/nginx/conf.d/default.conf

sed -i \
 -e "s|{ROOT_PATH}|$ROOT_PATH|g" \
 /etc/nginx/conf.d/default.conf

exec "$@"
