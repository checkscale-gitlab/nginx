> Auteur : Jordan Protin | jordan.protin@yahoo.com

# Docker image: Nginx

[![pipeline status](https://gitlab.com/jordanprotin/docker/nginx/badges/master/pipeline.svg)](https://gitlab.com/jordanprotin/docker/nginx/commits/master)

# Sommaire

- [Introduction](#introduction)
- [Tags actuellement disponibles](#tags-actuellement-disponibles)
- [Configuration](#configuration)

# Introduction 

Il s'agit d'une image docker construite à partir de l'image officielle [nginx](https://hub.docker.com/_/nginx/).

> :+1: Toute la [configuration](#configuration) peut être paramétrée en utilisant les variables d'environnements.

Les changements sont appliquées sans devoir reconstruire l'image.

# Tags actuellement disponibles

> **Pull command** : 
> docker pull [registry.gitlab.com/jordanprotin/docker/nginx:1.15](https://gitlab.com/jordanprotin/docker/nginx/container_registry)

* 1.15 (latest)

## Configuration

La configuration peut-être paramétrée en passant les variables d'environnements (cf. tableau ci-dessous) en paramètres de la commande suivante  :

```bash
$ docker run -e DISALLOW_ROBOTS=false -d registry.gitlab.com/jordanprotin/docker/nginx
```

| Nom de la variable        | Description                                                        | Valeur par défaut |
|-----------------|--------------------------------------------------------------------|---------------|
| ROOT_PATH        | Racine du document root (sans les /)| /usr/share/nginx/html |